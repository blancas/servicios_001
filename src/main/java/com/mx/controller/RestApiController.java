package com.mx.controller;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;   
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController; 
import org.json.JSONObject;

import com.mx.bean.City;
import com.mx.bean.CityService;
import com.mx.bean.LimitesUsuario;
import com.mx.bean.OperationResult;
import com.mx.bean.Principal;
import com.mx.bean.Response;
import com.mx.bean.User;
import com.mx.bean.Usuarios;
import com.mx.bean.UserService;
import com.mx.bean.UserServiceF;
import lombok.Data;
import io.swagger.annotations.ApiOperation;

import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

@Data
@RestController
@RequestMapping("/controllersf")
public class RestApiController 
{
	
    public static final Logger logger = LoggerFactory.getLogger(RestApiController.class);
    public String url     ="http://localhost:8083/";
    public String context ="services";
    public String path    ="/api/buro/identidad";
    @Autowired
    UserService userService; //Service which will do all data retrieval/manipulation work }
    @Autowired
    UserServiceF userServiceOauth;
    @Autowired 
	private CityService cityService;  //Service which will do all data retrieval/manipulation workx
    
    @RequestMapping(value = "/procUser", method = RequestMethod.GET)
    public ResponseEntity<List<User>> getProcUser() 
    {
    	logger.info("Peticion{"+url+path+"/procUser}");
    	
        List<User> usuarios = userServiceOauth.findAll();
        if (usuarios.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT); 
        }
        return new ResponseEntity<List<User>>(usuarios, HttpStatus.OK);
    }
 
 
    // -------------------valida servicio---------------------------------------------
    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<OperationResult> valida() 
    {
    	logger.info("Peticion{"+url+path+"/}");
    	OperationResult op=new OperationResult(true,"Conexion con servicios y Base de datos con exito");
        return new ResponseEntity<OperationResult>(op, HttpStatus.OK);
    }
    
    
    // -------------------Retrieve All cities---------------------------------------------
   
    @RequestMapping(value = "/cities/", method = RequestMethod.GET)
    public ResponseEntity<List<City>> listCities() 
    {
    	logger.info("Peticion{"+url+path+"/cities/}");
    	
        List<City> cities = cityService.findAll();
        if (cities.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT); 
        }
        return new ResponseEntity<List<City>>(cities, HttpStatus.OK);
    }
    // -------------------Retrieve Add city---------------------------------------------
    
    @RequestMapping(value = "/addCity/{name}/{population}", method = RequestMethod.PUT)
    public ResponseEntity<City> addCity(@PathVariable("name") String name, @PathVariable("population") int population) 
    {
    	logger.info("Peticion{"+url+path+"/addCity/}");
    	City nuevo=new City();
    	nuevo.setName(name);
    	nuevo.setPopulation(population);
    	nuevo =  cityService.save(nuevo);
        return new ResponseEntity<City>(nuevo, HttpStatus.OK);
    }
    // -------------------Retrieve modify only population---------------------------------------------
    
    @RequestMapping(value = "/modifyCityOP/{id}/{population}", method = RequestMethod.POST)
    public ResponseEntity<City> modifyCityOP(@PathVariable("id") long id, @PathVariable("population") int population) 
    {
    	logger.info("Peticion{"+url+path+"/modifyCity/}");
    	City modify1=new City();
    	modify1.setId(id);
    	modify1.setPopulation(population);
    	City modify2 = cityService.modify(modify1);
    	if(modify2==null)
    		return new ResponseEntity<City>(modify2, HttpStatus.BAD_REQUEST);
    	modify2.setPopulation(population);
    	cityService.save(modify2);
        return new ResponseEntity<City>(modify2, HttpStatus.OK);
    }
    // -------------------Retrieve modify city only Name---------------------------------------------
    
    @RequestMapping(value = "/modifyCityON/{id}/{name}", method = RequestMethod.POST)
    public ResponseEntity<City> modifyCityON(@PathVariable("id") long id,@PathVariable("name") String name) 
    {
    	logger.info("Peticion{"+url+path+"/modifyCity/}");
    	City modify1=new City();
    	modify1.setId(id);
    	modify1.setName(name);
    	City modify2 = cityService.modify(modify1);
    	if(modify2==null)
    		return new ResponseEntity<City>(modify2, HttpStatus.BAD_REQUEST);
    	modify2.setName(name);
    	cityService.save(modify2);
        return new ResponseEntity<City>(modify2, HttpStatus.OK);
    }
    @RequestMapping(value = "/deleteCity/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteCity(@PathVariable("id") long id) 
    {
    	logger.info("Peticion{"+url+path+"/deleteCity/}");
    	City delete1=new City();
    	delete1.setId(id);
    	boolean dato = cityService.delete(delete1);
        return new ResponseEntity<String>((new Boolean(dato)).toString(), HttpStatus.OK);
    }
    
    // -------------------Retrieve All Users---------------------------------------------
    
    @RequestMapping(value = "/users/", method = RequestMethod.GET)
    public ResponseEntity<List<Usuarios>>  listAllUsers() 
    {
    	logger.info("Peticion{"+url+path+"/users/}");
        List<Usuarios> users = userService.findAllUsers();
        if (users.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT); 
        }
        return new ResponseEntity<List<Usuarios>>(users, HttpStatus.OK);
    }
    
 
    // -------------------Retrieve Single User------------------------------------------
 
    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public ResponseEntity<Usuarios> getUser(@PathVariable("id") long id) 
    {
    	logger.info("Peticion{"+url+path+"/user/"+id+"}");
        logger.info("Fetching User with id {}", id);
        Usuarios user = userService.findById(id);
        if (user == null) 
        {
            logger.error("User with id {} not found.", id);
            return new ResponseEntity(user, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Usuarios>(user, HttpStatus.OK);
    }
    //--------------------Recibe OCR----------------------------------------

    @RequestMapping(value = "/ocr/karalundi/credentials",
    		method = RequestMethod.POST,
    		produces = MediaType.APPLICATION_JSON_UTF8_VALUE, 
    		consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<OperationResult> credentialOperation (
    @RequestPart(value = "json") String credentialsOperationRequest,
    @RequestPart(value = "file", required = false) List<MultipartFile> files)
    {
    	logger.info("Peticion{"+url+path+"/ocr/karalundi/credentials}");
    	logger.info("User with id::::"+ credentialsOperationRequest); 
    	JSONObject credentialsOperationRequestJSON = new JSONObject(credentialsOperationRequest);
        Long operationId = credentialsOperationRequestJSON.getLong("operationId");
        String cadena ="";
        int total=0;
        if(files.size()>0)
        {
        	for(int i=0;i<files.size();i++)
        		if(files.get(i)!=null)
        			total++;
        }
        boolean dato=true;
        if(operationId>0)
        	cadena = ("[{\"operationid\":"+operationId+",\"estatus\":\"ok\"}");
        else
        	{cadena = ("[{\"estatus\":\"Error\",\"mensaje\":\"Es necesario operarionId\"}");dato =false;}
        if(total>0)
        	cadena = cadena + (",{\"files.size\": "+total+",\"estatus\":\"ok\"}]");
        else
        	{cadena =  cadena + ("{\"estatus\":\"Error\",\"mensaje\":\"Es necesario las images\"}]");dato=false;}
        
        return new ResponseEntity<OperationResult>(new OperationResult(dato,cadena),
        		dato?HttpStatus.OK:HttpStatus.FAILED_DEPENDENCY);
    }
    //--------------------Recibe Facial----------------------------------------

    @RequestMapping(value = "/biometricos/karalundi/facial",
    		method = RequestMethod.POST,
    		produces = MediaType.APPLICATION_JSON_UTF8_VALUE, 
    		consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<OperationResult> facialOperation (
    @RequestPart(value = "json") String credentialsOperationRequest,
    @RequestPart(value = "file", required = false) List<MultipartFile> files)
    {
    	logger.info("Peticion{"+url+path+"/biometricos/karalundi/facial}");
    	logger.info("User with id::::"+ credentialsOperationRequest); 
    	JSONObject credentialsOperationRequestJSON = new JSONObject(credentialsOperationRequest);
        Long operationId = credentialsOperationRequestJSON.getLong("operationId");
        String cadena ="";
        int total=0;
        if(files.size()>0)
        {
        	for(int i=0;i<files.size();i++)
        		if(files.get(i)!=null)
        			total++;
        }
        boolean dato=true;
        if(operationId>0)
        	cadena = ("[{\"operationid\":"+operationId+",\"estatus\":\"ok\"}");
        else
        	{cadena = ("[{\"estatus\":\"Error\",\"mensaje\":\"Es necesario operarionId\"}");dato =false;}
        if(total>0)
        	
        	cadena = cadena + (",{\"files.size\": "+total+",\"estatus\":\"ok\"}]");
        else
        	{cadena =  cadena + ("{\"estatus\":\"Error\",\"mensaje\":\"Es necesario las images\"}]");dato=false;}
        
        return new ResponseEntity<OperationResult>(new OperationResult(dato,cadena),
        		dato?HttpStatus.OK:HttpStatus.FAILED_DEPENDENCY);
    }
  //--------------------Recibe Facial----------------------------------------

	@ApiOperation(value = "Adds the cyphered binary information from the capture of the fingers. "
			+ "Expects a JSON like {'operationId' : 1, 'scanId' : 'currentScan' , 'documentId' : 'currentDocumentManagerId' ,"
			+ " 'll' : 'x' , 'lr' : 'x', 'lm' : 'x' , 'li': 'x', 'lt' : 'x', "
			+ "'rl' : 'x', 'rr' : 'x', 'rm' : 'x', 'ri' : 'x'} "
			+ "as first 'l' stands for left and  'r' for right "
			+ "and second letter stands for little, ring, middle, index and thumb")
    @RequestMapping(value = "/biometricos/karalundi/huellas", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    public ResponseEntity<OperationResult> addMinuciasCyphered(
            @RequestPart(value = "json") String jsonRequest, HttpServletRequest request) 
	{
		logger.info("Peticion{"+url+path+"/biometricos/karalundi/huellas}");
		logger.info("Valida :left , right  [little, ring, middle, index and thumb,] ---> {\"valida\":\"0001000010\"}");
    	logger.info("User with id::::"+ jsonRequest); 
    	JSONObject credentialsOperationRequestJSON = new JSONObject(jsonRequest);
        Long operationId = credentialsOperationRequestJSON.getLong("operationId");
        
		boolean dato=true;
		String cadena ="";
		
        int total=10;
        if(operationId>0)
        	cadena = ("[{\"operationid\":"+operationId+",\"estatus\":\"ok\"}");
        else
        	{cadena = ("[{\"estatus\":\"Error\",\"mensaje\":\"Es necesario operarionId\"}");dato =false;}
        if(total>0)
        	cadena = cadena + (",{\"files.size\": "+total+",\"estatus\":\"ok\"}]");
        else
        	{cadena =  cadena + ("{\"estatus\":\"Error\",\"mensaje\":\"Es necesario las images\"}]");dato=false;}
		return new ResponseEntity<OperationResult>(new OperationResult(dato,cadena),
        		dato?HttpStatus.OK:HttpStatus.FAILED_DEPENDENCY);
    }
    // ------------------- Delete All Users-----------------------------
 
    @RequestMapping(value = "/userborrado", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteAllUsers(@PathVariable("id") long id) {
    	logger.info("Peticion{"+url+path+"/userborrado/}");
        logger.info("Deleting All Users");
 
        userService.deleteAllUsers();
        return new ResponseEntity<String>("Borrado Exitoso",HttpStatus.NO_CONTENT);
    }
 // ------------------- Delete All Users-----------------------------
    
    @RequestMapping(value = "/userborrado/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteUser(@PathVariable("id") long id) {
    	logger.info("Peticion{"+url+path+"/userborrado/}");
        logger.info("Deleting  User");
 
        userService.deleteUser(id);
        return new ResponseEntity<String>("Borrado Exitoso ["+id+"]",HttpStatus.NO_CONTENT);
    }
    
    @RequestMapping(value = "/user/me", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Principal user(@RequestBody Principal principal) 
	{
        System.out.println(principal.toString());
        principal.setMensaje("Servicio Modificado."+principal.getMensaje());
        return principal;
    }
}
