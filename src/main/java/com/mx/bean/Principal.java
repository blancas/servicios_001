package com.mx.bean;

import java.io.Serializable;

public class Principal implements Serializable 
{
	private static final long serialVersionUID = 1L;
	private  Integer id; 
    private String mensaje;
    
	public Principal(Integer id, String mensaje) {
		super();
		this.id = id;
		this.mensaje = mensaje;
	}
    public Principal() 
    {
    }
    public Principal(String men) 
    {
    	mensaje = men;
    }
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
    
}
