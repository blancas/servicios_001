package com.mx.bean;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceF implements IUserServiceF
{
	@Autowired
    private UserFRepository repository;

	@Override
    public List<User> findAll() {

        List<User> usuarios = (List<User>) repository.findAll();
        
        return usuarios;
	    }
}
