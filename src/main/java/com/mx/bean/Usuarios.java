package com.mx.bean;

import java.io.Serializable;

public class Usuarios implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7356331868729558724L;
	private long id;
	private String name;
	private int age;
	private float salary;
	public Usuarios(long id1,String name1,int age1,float salary1)
	{
		this.id=id1;
		this.name=name1;
		this.age=age1;
		this.salary=salary1;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public float getSalary() {
		return salary;
	}
	public void setSalary(float salary) {
		this.salary = salary;
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", age=" + age + ", salary=" + salary + "]";
	}
	
}
