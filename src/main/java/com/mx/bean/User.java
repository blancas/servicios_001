package com.mx.bean;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;
@Entity
@Table(name = "user", schema = "public", catalog = "servicio")
public class User 
{
    private Long id;
	private String email;
	private String username;
	private String password;
	private Long version;
	private boolean enabled;
	private boolean account_locked;
	private boolean account_expired;
	private boolean credentials_expired; 
	private Timestamp created_on;
	private Timestamp updated_on;
	@Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) 
    {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return false;
    }
    
    @Id
    @Column(name = "id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

    @Basic
    @Column(name = "email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
    @Basic
    @Column(name = "username")
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

    @Basic
    @Column(name = "password")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

    @Basic
    @Column(name = "enabled")
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

    @Basic
    @Column(name = "account_locked")
	public boolean isAccount_locked() {
		return account_locked;
	}

	public void setAccount_locked(boolean account_locked) {
		this.account_locked = account_locked;
	}

    @Basic
    @Column(name = "account_expired")
	public boolean isAccount_expired() {
		return account_expired;
	}

	public void setAccount_expired(boolean account_expired) {
		this.account_expired = account_expired;
	}

    @Basic
    @Column(name = "credentials_expired")
	public boolean isCredentials_expired() {
		return credentials_expired;
	}

	public void setCredentials_expired(boolean credentials_expired) {
		this.credentials_expired = credentials_expired;
	}

    @Basic
    @Column(name = "version")
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}


    @Basic
    @Column(name = "created_on")
	public Timestamp getCreated_on() {
		return created_on;
	}

	public void setCreated_on(Timestamp created_on) {
		this.created_on = created_on;
	}


    @Basic
    @Column(name = "updated_on")
	public Timestamp getUpdated_on() {
		return updated_on;
	}

	public void setUpdated_on(Timestamp updated_on) {
		this.updated_on = updated_on;
	}
	
	
	 
 
    
/*
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "role_user",
		joinColumns = { @JoinColumn(name = "user_id", referencedColumnName = "id") },
		inverseJoinColumns = { @JoinColumn(name = "role_id", referencedColumnName = "id") })
	private List<Role> roles;
*/
	 

	/*
	 * Get roles and permissions and add them as a Set of GrantedAuthority
	 */
	/*
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();

		roles.forEach(r -> {
			authorities.add(new SimpleGrantedAuthority(r.getName()));
			r.getPermissions().forEach(p -> {
				authorities.add(new SimpleGrantedAuthority(p.getName()));
			});
		});

		return authorities;
	}
	*/
	
}
