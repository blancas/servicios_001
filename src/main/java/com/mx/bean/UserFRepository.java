package com.mx.bean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserFRepository  extends JpaRepository<User, Long>
{ 

}
