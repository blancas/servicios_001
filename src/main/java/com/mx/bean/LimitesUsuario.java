package com.mx.bean;

import java.io.Serializable;

public class LimitesUsuario implements Serializable 
{
	long idUsuario;
	String idPersonaFisica;
	String nombre;
	String segundoNombre;
	String apellidoPaterno;
	String apellidoMaterno;
	String alias;
	float limiteIndividual;
	float limiteMancomunado;
	float limiteOperable;
	String rangoOperable;
	String rfc;	
	String telefono;	
	String email;	
	boolean activo;	
	boolean maestro;
	Dispositivo dispositivo;
	
	public LimitesUsuario(long idUsuario1, String idPersonaFisica1, String nombre1, String segundoNombre1,
			String apellidoPaterno1, String apellidoMaterno1, String alias1, float limiteIndividual1,
			float limiteMancomunado1, float limiteOperable1, String rangoOperable1, String rfc1, String telefono1,
			String email1, boolean activo1, boolean maestro1) {
		this.idUsuario= idUsuario1;
		this.idPersonaFisica = idPersonaFisica1;
		this.nombre = nombre1;
		this.segundoNombre = segundoNombre1;
		this.apellidoPaterno= apellidoPaterno1;
		this.apellidoMaterno = apellidoMaterno1;
		this.alias = alias1;
		this.limiteIndividual = limiteIndividual1;
		this.limiteMancomunado = limiteMancomunado1;
		this.limiteOperable = limiteOperable1;
		this.rangoOperable = rangoOperable1;
		this.rfc = rfc1;	
		this.telefono = telefono1;	
		this.email= email1;	
		this.activo= activo1;	
		this.maestro = maestro1;
	}
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getIdPersonaFisica() {
		return idPersonaFisica;
	}
	public void setIdPersonaFisica(String idPersonaFisica) {
		this.idPersonaFisica = idPersonaFisica;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getSegundoNombre() {
		return segundoNombre;
	}
	public void setSegundoNombre(String segundoNombre) {
		this.segundoNombre = segundoNombre;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public float getLimiteIndividual() {
		return limiteIndividual;
	}
	public void setLimiteIndividual(float limiteIndividual) {
		this.limiteIndividual = limiteIndividual;
	}
	public float getLimiteMancomunado() {
		return limiteMancomunado;
	}
	public void setLimiteMancomunado(float limiteMancomunado) {
		this.limiteMancomunado = limiteMancomunado;
	}
	public float getLimiteOperable() {
		return limiteOperable;
	}
	public void setLimiteOperable(float limiteOperable) {
		this.limiteOperable = limiteOperable;
	}
	public String getRangoOperable() {
		return rangoOperable;
	}
	public void setRangoOperable(String rangoOperable) {
		this.rangoOperable = rangoOperable;
	}
	public String getRfc() {
		return rfc;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public boolean isActivo() {
		return activo;
	}
	public void setActivo(boolean activo) {
		this.activo = activo;
	}
	public boolean isMaestro() {
		return maestro;
	}
	public void setMaestro(boolean maestro) {
		this.maestro = maestro;
	}
	public Dispositivo getDispositivo() {
		return dispositivo;
	}
	public void setDispositivo(Dispositivo dispositivo) {
		this.dispositivo = dispositivo;
	}

	
}
