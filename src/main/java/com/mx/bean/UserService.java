package com.mx.bean;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class UserService implements IUserService {

	public boolean isUserExist(Usuarios us)
	{
		return true;
	}
	public List<Usuarios> findAllUsers()
	{
		Usuarios us1=new Usuarios(1,"Luis",23,2455.98f);
		Usuarios us2=new Usuarios(2,"Carlos",24,3456.98f);
		Usuarios us3=new Usuarios(3,"Victor",25,4556.98f);
		List<Usuarios> li=new ArrayList<Usuarios>();
		li.add(us1);
		System.out.println("findAllUsers >>"+us1.toString());
		li.add(us2);
		System.out.println("findAllUsers >>"+us2.toString());
		li.add(us3);
		System.out.println("findAllUsers >>"+us3.toString());
		return li;
	}
	public void deleteUserById(long id)
	{ 
		Usuarios us3=new Usuarios(id,"Carlos",26,6756.98f);
		System.out.println("deleteUserById >>"+us3.toString());
	}
	public Usuarios findById(long id)
	{
		Usuarios us3=new Usuarios(id,"Carlos",26,6756.98f);
		System.out.println("findById >>"+us3.toString());
		return us3;
	}
	public void saveUser(Usuarios usr)
	{
		System.out.println("saveUser >>"+usr.toString());
	}
	public void updateUser(Usuarios usr)
	{ 
	}
	public void deleteAllUsers()
	{
		System.out.println("deleteAllUsers >>");
	}
	public void deleteUser(long i)
	{
		System.out.println("deleteAllUser >>" + i);
	}
}
