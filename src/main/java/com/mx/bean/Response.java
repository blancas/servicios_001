package com.mx.bean;

import java.util.List;

public class Response {
	private long responseStatus;
	private String responseError;
	private List<LimitesUsuario> result;
	public Response(long responseStatus1, String responseError1) {
		this.responseStatus=responseStatus1;
		this.responseError=responseError1;
	}
	public long getResponseStatus() {
		return responseStatus;
	}
	public void setResponseStatus(long responseStatus) {
		this.responseStatus = responseStatus;
	}
	public String getResponseError() {
		return responseError;
	}
	public void setResponseError(String responseError) {
		this.responseError = responseError;
	}
	public List<LimitesUsuario> getResult() {
		return result;
	}
	public void setResult(List<LimitesUsuario> result) {
		this.result = result;
	}
	
}
