package com.mx.bean;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CityService implements ICityService {

    @Autowired
    private CityRepository repository;

    @Override
    public List<City> findAll() {

        List<City> cities = (List<City>) repository.findAll();
        
        return cities;
    }
    @Transactional
	public City save(City nuevo) 
    {
		return repository.save(nuevo);
	}
	public City modify(City n) 
	{
		City  modif=repository.findOne(n.getId());
		if(modif ==  null)
			return null;
		return modif;
	}
	@Transactional
	public boolean delete(City de) 
    {
		City  delete=repository.findOne(de.getId());
		if(delete ==  null)
			return false;
		repository.delete(delete);
		return true;
	}
}
