package com.mx.bean;

public class OperationResult {
	private boolean resultOK;
	private String errorMessage;
	public boolean isResultOK() {
		return resultOK;
	}
	public void setResultOK(boolean resultOK) {
		this.resultOK = resultOK;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public OperationResult(boolean resultOK, String errorMessage) {
		super();
		this.resultOK = resultOK;
		this.errorMessage = errorMessage;
	}
	
}
