package com.mx.bean;

public class Dispositivo {
	private long idDispositivo;
	private String noSerie;
	private long idCaja;
	
	public Dispositivo(long idDispositivo, String noSerie, long noCaja) {
		super();
		this.idDispositivo = idDispositivo;
		this.noSerie = noSerie;
		this.idCaja = noCaja;
	}
	public long getIdDispositivo() {
		return idDispositivo;
	}
	public void setIdDispositivo(long idDispositivo) {
		this.idDispositivo = idDispositivo;
	}
	public String getNoSerie() {
		return noSerie;
	}
	public void setNoSerie(String noSerie) {
		this.noSerie = noSerie;
	}
	public long getIdCaja() {
		return idCaja;
	}
	public void setIdCaja(long idCaja) {
		this.idCaja = idCaja;
	}
	
}
