package com.mx.bean;
import java.util.List;
public interface IUserService 
{
		public boolean isUserExist(Usuarios us);
		public List<Usuarios> findAllUsers();
		public void deleteUserById(long id);
		public Usuarios findById(long id);
		public void saveUser(Usuarios usr);
		public void updateUser(Usuarios usr);
		public void deleteAllUsers();
		public void deleteUser(long i);

}
